#!/usr/bin/env bash

image() {
  local size=$1
  local color=$2
  local name=$3
  convert -size $size xc:${color} images/${name}.png
}

image 1024x768 black background
image 600x400  red   rectangle
